package api;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.matcher.RestAssuredMatchers.* ;
import static org.hamcrest.Matchers.* ;
/**
 * 
 * @author Ruba
 *
 */
public class Get {

	/** 
	 * to test the status code
	 */
	@Test
	void testStatus() {
//		RestAssured.registerParser("")
		given().accept("application/json").get("http://192.168.200.91:8080/demo-server/employee-module/RUBAW").then()
		.statusCode(200);
	}
	/** 
	 * to test body if it same as actual
	 */
	@Test
		void test() {
		given().accept("application/json").get("http://192.168.200.91:8080/demo-server/employee-module/RUBAW").then()
		.body("maxSalary",equalTo(10000))
		.body("totalSalaries",equalTo(15000))
		.body("employeesCount",equalTo(2))
		.body("minSalary",equalTo(5000));	
	}
		
		/** 
		 * to test items in the body if they are same as actual
		 */
	@Test
		void testitems2() {
		  given().accept("application/json").get("http://192.168.200.91:8080/demo-server/employee-module/RUBAW").then()	
		  .body("items.firstName[0]", equalTo("Blaise"))
	      .body("items.lastName[0]", equalTo("Pascal"))
	      .body("items.id[0]", equalTo("438745745094"))
	      .body("items.salary[0]", equalTo(10000))
	      .body("items.firstName[1]", equalTo("John"))
	      .body("items.lastName[1]", equalTo("Smith"))
	      .body("items.id[1]", equalTo("438768422146"))
	      .body("items.salary[1]", equalTo(5000));
	}
	
}