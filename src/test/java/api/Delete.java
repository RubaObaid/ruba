package api;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.Response;


public class Delete extends BaseTest{
	@Test
	 void validDelete_1() {
	 //add data
		String jsonUser = Util.getFile("json/user.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
			contentType(ContentType.JSON).
		when().
			post("http://192.168.200.91:8080/demo-server/employee-module/u");
//delete data
		given().
		contentType(ContentType.JSON).
		when().
			delete("http://192.168.200.91:8080/demo-server/employee-module/u/5").
		then().
			statusCode(200);
//		assertThat().
		//	body("status", equalTo("SUCCESS")).
		//	body("message", equalTo("New employee is added successfully."));
	 }
	
	@Test
	 void notExistingId_2() {
	 //add data
		String jsonUser = Util.getFile("json/user.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
			contentType(ContentType.JSON).
		when().
			post("http://192.168.200.91:8080/demo-server/employee-module/u");
//delete data
		given().
		contentType(ContentType.JSON).
		when().
			delete("http://192.168.200.91:8080/demo-server/employee-module/u/9").
		then().
			statusCode(200);
//		assertThat().
		//	body("status", equalTo("SUCCESS")).
		//	body("message", equalTo("New employee is added successfully."));
	 }
	
	
	@Test
	 void allDelete_3() {

		given().
		contentType(ContentType.JSON).
		when().
			delete("http://192.168.200.91:8080/demo-server/employee-module/u").
		then().
			statusCode(200);
//		assertThat().
		//	body("status", equalTo("SUCCESS")).
		//	body("message", equalTo("New employee is added successfully."));
		
		 //add data
			String jsonUser = Util.getFile("json/get.json").toString();
			given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
				body(jsonUser).
				contentType(ContentType.JSON).
			when().
				post("http://192.168.200.91:8080/demo-server/employee-module/u");
	 }
	

	@Test
	void testStatus_4() {
		String jsonUser = Util.getFile("json/user.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
			contentType(ContentType.JSON).
		when().
			post("http://192.168.200.91:8080/demo-server/employee-module/u");
		//delete data
				given().
				contentType(ContentType.JSON).
				when().
					delete("http://192.168.200.91:8080/demo-server/employee-module/u/1").
				then().
					statusCode(200);
	}
	
}