package api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Util {
	
public static JSONObject getFile(String fileName) {
	JSONParser parser = new JSONParser();
	ClassLoader classLoader = Util.class.getClassLoader();
	JSONObject jsonObject = new JSONObject();
	
	try {
		InputStream inputStream = classLoader.getResourceAsStream(fileName);
		jsonObject= (JSONObject) parser.parse(new InputStreamReader(inputStream, "UTF-8"));
	}
	catch(FileNotFoundException e) {
		e.printStackTrace();
	}
	catch(IOException e) {
		e.printStackTrace();
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	return jsonObject;
}
}