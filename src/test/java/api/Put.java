package api;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;


public class Put extends BaseTest{
	
	@Test
	void validPut_1() {

		JSONObject json = new JSONObject();
		json.put("title", "amal");
		json.put("author", "ah");
		given().
			header("Content-Type", "application/json").
			body(json.toJSONString()).
		when().
			put("http://localhost:3000/posts/1").
		then().
			statusCode(200);
	//	assertThat().
	//	body("status", equalTo("SUCCESS")).
	//	body("message", equalTo("New employee is added successfully."));
	}
	
	
	@Test
	void notExistingId_2() {
		JSONObject json = new JSONObject();
		json.put("title", "amal");
		json.put("author", "ah");
		given().
			header("Content-Type", "application/json").
			body(json.toJSONString()).
		when().
			put("http://localhost:3000/posts/7").
		then().
			statusCode(404);
	//	assertThat().
	//	body("status", equalTo("SUCCESS")).
	//	body("message", equalTo("New employee is added successfully."));
	}

	
	@Test
	void emptyId_3() {
		JSONObject json = new JSONObject();
		json.put("title", "amal");
		json.put("author", "ah");
		given().
			header("Content-Type", "application/json").
			body(json.toJSONString()).
		when().
			put("http://localhost:3000/posts").
		then().
			statusCode(404);
	//	assertThat().
	//	body("status", equalTo("SUCCESS")).
	//	body("message", equalTo("New employee is added successfully."));
	}
	
	@Test
	void testStatus_4() {
		JSONObject json = new JSONObject();
		json.put("title", "amal");
		json.put("author", "ah");
		given().
			header("Content-Type", "application/json").
			body(json.toJSONString()).
		when().
			put("http://localhost:3000/posts/1").
		then().
			statusCode(200);
	}
	
	
}