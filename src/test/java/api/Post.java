package api;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.Response;
/**
 * 
 * @author Ruba
 *
 */
public class Post extends BaseTest{

	@Test
	void validPost_1() {
		/**
		 * take information from file
		 */
		String jsonUser = Util.getFile("json/user.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		body(jsonUser).
		contentType(ContentType.JSON).
		when().
		/**
		 * add the information to the API
		 */
		post("http://192.168.200.91:8080/demo-server/employee-module/n").
		then().
		statusCode(200).
		assertThat().
		body("status", equalTo("SUCCESS")).
		body("message", equalTo("New employee is added successfully."));

		/**
		 * delete the employee
		 */
		given().
		contentType(ContentType.JSON).
		delete("http://192.168.200.91:8080/demo-server/employee-module/n/1");
	}



	@Test
	void existsId_2() {
		/**
		 * take information from file
		 */
		String json = Util.getFile("json/exists.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		body(json).
		contentType(ContentType.JSON).
		when().
		post("http://192.168.200.91:8080/demo-server/employee-module/n").
		then().
		statusCode(200).
		assertThat().
		body("status", equalTo("ERROR")).
		body("message", equalTo("Employee already exists."));
	}

	@Test
	void emptyId_3() {
		/**
		 * take information from file
		 */
		String json = Util.getFile("json/empty.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		body(json).
		contentType(ContentType.JSON).
		when().
		post("http://192.168.200.91:8080/demo-server/employee-module/n").
		then().
		statusCode(200).
		assertThat().
		body("status", equalTo("ERROR")).
		body("message", equalTo("Invalid value for employee id."));
	}
	
	
	/*
	void testStatus_4() {
		String jsonUser = Util.getFile("json/user.json").toString();
		given().
		headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		body(jsonUser).
		contentType(ContentType.JSON).
		when().
		
		post("http://192.168.200.91:8080/demo-server/employee-module/n").
		then().
		assertThat().
		statusCode(200);
	
		given().
		contentType(ContentType.JSON).
		delete("http://192.168.200.91:8080/demo-server/employee-module/n/1");
	}

*/
}